## **Sample Code**
This is my sample code for my application.

### PHP
- I choose to generate a report from raw data of attendance log. 
- Data named attlog.dat was in php folder. It was generated from biometrics.

### Bootstrap
- I created bootstrap template to display the generated data.
- I uploaded it in [00webhosting.com](https://sample-code.000webhostapp.com/) to show it's working.
