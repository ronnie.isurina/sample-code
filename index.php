<?php
     function __autoload($class){ 
         require_once './php/'.$class.'.php'; 
    }
    
    $logs = new ReportGenerator;
    $report = $logs->generate();
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bootstrap - Sample Code</title>

    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="form-validation.css" rel="stylesheet">
  </head>

  <body class="bg-light">

    <div class="container">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" 
            src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" 
            alt="" 
            width="72" 
            height="72">
        <h2>Bootstrap - Sample Code</h2>
      </div>

      <div class="row justify-content-center">
        <div class="col-md-8 order-md-1">
            <h4 class="mb-3">Attendance Report</h4>
            
            <table class="table ">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Time IN</th>
                        <th>Time OUT</th>
                    </tr>
                </thead>

                <tbody>
                <?php foreach( $report as $log ): ?>
                    <tr>
                        <td><?php echo $log['id'] ?></td>
                        <td><?php echo $log['date'] ?></td>
                        <td><?php echo $log['log']['time_in'] ?></td>
                        <td><?php echo $log['log']['time_out'] ?></td>
                    </tr>
                <?php endforeach?>
                </tbody>
            </table>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
  </body>
</html>
