<?php

class ReportGenerator {

    private $logs;

    public function __construct( $logs = null ) {
        $this->logs = $logs;

        if( !$logs ) {
            $file = file_get_contents('attlog.dat', FILE_USE_INCLUDE_PATH);
            $this->logs = $file;
        }
    }

    /**
     * Turn file text data to array
     *
     * @return array
     */
    public function generate() {
        $logs = $this->logs_to_array();
        #check logs
        if ( empty( $logs ) ) {
            return false;
        }

        $result = [];
        # filter by ID and date
        foreach ( $logs as $id => $log) {
            foreach( $log as $date => $times) {
                $result[] = [
                    'id' => $id,
                    'date' => $date,
                    'log' => $this->get_time_in_out($times),
                ];
            }
        }

        return $result;
    }

    
    /**
     * Determine if log was in or out
     *
     * @param array
     * @return array
     */
    protected function get_time_in_out( $times ) {
        if(!$times) { return false; }

        $time_in = '';
        $time_out = '';
        $time_count = count($times);
        
        if ($time_count == 1) {
            $time_in = $times[0];
        }else{
            $time_in = $times[0];
            $time_out = $times[($time_count-1)];
        }

        return [
            'time_in' => $time_in,
            'time_out' => $time_out
        ];
    }

    /**
     * Turn file text data to array
     *
     * @return array
     */
    protected function logs_to_array() {

        if ( empty( $this->logs ) ) {
            return false;
        }
        $data = [];
        foreach(explode("  ", $this->logs) as $log) {
            if(!empty($log) && $log != null) {
                $log_data = explode("	", $log); 

                if( isset($log_data[1]) ){
                    $id = preg_replace('/\s+/', '', $log_data[0]);

                    $date_time = explode(" ",$log_data[1]);
                    $date = $date_time[0];
                    $time = $date_time[1];
                    
                    $data[$id][$date][] = $time;
                }
            }
        }
        return $data;
    }
}
